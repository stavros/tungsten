package com.stochastictechnologies.tungsten;

import android.os.AsyncTask;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

public class ConfigDownloader extends AsyncTask<String, Integer, Long> {
    private String notification = "";
    private ArrayList<Pane> panes = new ArrayList<>();
    private TungstenActivity view;

    public ConfigDownloader(TungstenActivity view2) {
        this.view = view2;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Long result) {
        super.onPostExecute(result);
        if (this.panes.size() > 0) {
            Iterator<Pane> it = this.panes.iterator();
            while (it.hasNext()) {
                Pane pane = it.next();
                TungstenActivity.paneDB.addPane(pane);
                this.view.mCollectionPagerAdapter.panes.add(pane);
            }
            this.view.mCollectionPagerAdapter.notifyDataSetChanged();
            this.notification = this.view.getString(R.string.download_successful);
        } else if (this.notification.trim().equals("")) {
            this.notification = this.view.getString(R.string.no_parsed_panes);
        }
        Toast.makeText(this.view.getApplicationContext(), this.notification, Toast.LENGTH_LONG).show();
    }

    /* access modifiers changed from: protected */
    public Long doInBackground(String... urls) {
        try {
            HttpURLConnection urlConnection = (HttpURLConnection) new URL(urls[0]).openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("Content-length", "0");
            urlConnection.setUseCaches(false);
            urlConnection.setAllowUserInteraction(false);
            urlConnection.setConnectTimeout(30000);
            urlConnection.setReadTimeout(30000);
            try {
                urlConnection.connect();
                int status = urlConnection.getResponseCode();
                switch (status) {
                    case 200:
                    case 201:
                        BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                        StringBuilder sb = new StringBuilder();
                        while (true) {
                            String line = br.readLine();
                            if (line == null) {
                                br.close();
                                this.panes = new Parser().parseFile(sb.toString());
                                break;
                            } else {
                                sb.append(String.valueOf(line) + "\n");
                            }
                        }
                    default:
                        this.notification = String.format(this.view.getString(R.string.invalid_server_code), new Object[]{Integer.valueOf(status)});
                        break;
                }
            } catch (RuntimeException e) {
                this.notification = this.view.getString(R.string.yaml_error);
            } catch (Exception e2) {
                this.notification = e2.getMessage();
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e3) {
            this.notification = e3.getMessage();
        }
        return null;
    }
}
