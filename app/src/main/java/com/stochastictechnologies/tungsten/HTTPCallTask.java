package com.stochastictechnologies.tungsten;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPCallTask extends AsyncTask<String, Integer, Long> {
    Context context;
    String notification = "";

    public HTTPCallTask(Context context2) {
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Long result) {
        super.onPostExecute(result);
        if (this.notification == null) {
            this.notification = this.context.getString(R.string.generic_error);
        }
        if (!this.notification.trim().equals("")) {
            Toast.makeText(this.context, this.notification, Toast.LENGTH_LONG).show();
        }
    }

    /* access modifiers changed from: protected */
    public Long doInBackground(String... urls) {
        try {
            HttpURLConnection urlConnection = (HttpURLConnection) new URL(urls[0]).openConnection();
            urlConnection.setUseCaches(false);
            urlConnection.setAllowUserInteraction(false);
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(10000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setFixedLengthStreamingMode(0);
            try {
                urlConnection.connect();
                switch (urlConnection.getResponseCode()) {
                    case 200:
                    case 201:
                        BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                        StringBuilder sb = new StringBuilder();
                        while (true) {
                            String line = br.readLine();
                            if (line == null) {
                                br.close();
                                this.notification = sb.toString().trim();
                                break;
                            } else {
                                sb.append(String.valueOf(line) + "\n");
                            }
                        }
                }
            } catch (Exception e) {
                this.notification = e.getMessage();
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e2) {
            this.notification = e2.getMessage();
        }
        return null;
    }
}
