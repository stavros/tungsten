package com.stochastictechnologies.tungsten;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;

public class Panes extends SQLiteOpenHelper {
    private static final String COL_ID = "id";
    private static final String COL_TITLE = "title";
    private static final String COL_YAML = "yaml";
    private static final String DATABASE_NAME = "paneManager";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_PANES = "panes";

    public Panes(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE panes (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title TEXT NOT NULL, yaml TEXT NOT NULL);");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS panes");
        onCreate(db);
    }

    /* access modifiers changed from: package-private */
    public void addPane(Pane pane) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COL_TITLE, pane.title);
        values.put(COL_YAML, pane.yaml);
        db.insert(TABLE_PANES, (String) null, values);
        db.close();
    }

    /* access modifiers changed from: package-private */
    public Pane getByPosition(int position) {
        Cursor cursor = getReadableDatabase().query(TABLE_PANES, new String[]{COL_ID, COL_TITLE, COL_YAML}, (String) null, (String[]) null, (String) null, (String) null, (String) null, " " + String.valueOf(position) + ", 1");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return new Pane(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
    }

    /* access modifiers changed from: package-private */
    public Pane getPane(int id) {
        Cursor cursor = getReadableDatabase().query(TABLE_PANES, new String[]{COL_ID, COL_TITLE, COL_YAML}, "id= ?", new String[]{String.valueOf(id)}, (String) null, (String) null, (String) null, (String) null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return new Pane(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
    }

    public ArrayList<Pane> getAll() {
        ArrayList<Pane> panes = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT  * FROM panes", (String[]) null);
        if (cursor.moveToFirst()) {
            do {
                Pane pane = new Pane();
                pane.id = Integer.parseInt(cursor.getString(0));
                pane.title = cursor.getString(1);
                pane.yaml = cursor.getString(2);
                panes.add(pane);
            } while (cursor.moveToNext());
        }
        db.close();
        return panes;
    }

    public int updatePane(Pane pane) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COL_TITLE, pane.title);
        values.put(COL_YAML, pane.yaml);
        int result = db.update(TABLE_PANES, values, "id = ?", new String[]{String.valueOf(pane.id)});
        db.close();
        return result;
    }

    public void delete(int id) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_PANES, "id = ?", new String[]{String.valueOf(id)});
        db.close();
    }

    public void deleteByPosition(int position) {
        Pane pane = getByPosition(position);
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_PANES, "id = ?", new String[]{String.valueOf(pane.id)});
        db.close();
    }

    public void deleteAll() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_PANES, (String) null, (String[]) null);
        db.close();
    }

    public int size() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT count(*) FROM panes", (String[]) null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        cursor.close();
        db.close();
        return count;
    }
}
