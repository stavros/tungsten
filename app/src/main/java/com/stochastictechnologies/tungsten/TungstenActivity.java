package com.stochastictechnologies.tungsten;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.PagerTitleStrip;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;
import java.util.List;

public class TungstenActivity extends AppCompatActivity {
    private static final String TAG = "TungstenActivity";
    static Context appContext;
    static Panes paneDB;
    CollectionPagerAdapter mCollectionPagerAdapter;
    ViewPager mViewPager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            appContext = this;
            setContentView(R.layout.main);
            paneDB = new Panes(this);
            this.mCollectionPagerAdapter = new CollectionPagerAdapter(getSupportFragmentManager());
            this.mViewPager = findViewById(R.id.pager);
            this.mViewPager.setOffscreenPageLimit(2);
            this.mViewPager.setAdapter(this.mCollectionPagerAdapter);
        } catch (Exception e) {
            Log.e(TAG, "Error in onCreate", e);
            Toast.makeText(this, "An error occurred while starting the app", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public void onAddPane(MenuItem item) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.add_pane);
        alert.setMessage(R.string.download_message);
        final EditText input = new EditText(this);
        input.setInputType(17);
        input.setHint(R.string.add_pane_hint);
        alert.setView(input);
        alert.setPositiveButton(R.string.button_add, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String url = input.getText().toString().trim();
                if (!url.equals("")) {
                    if (!url.contains("://")) {
                        url = "http://" + url;
                    }
                    new ConfigDownloader(TungstenActivity.this).execute(new String[]{url});
                }
            }
        });
        alert.setNegativeButton(R.string.label_cancel, (DialogInterface.OnClickListener) null);
        alert.show();
    }

    public void onDeletePane(MenuItem item) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.delete_pane);
        alert.setMessage(R.string.delete_pane_confirmation_body);
        alert.setPositiveButton(R.string.label_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                int position = TungstenActivity.this.mViewPager.getCurrentItem();
                if (position < TungstenActivity.this.mCollectionPagerAdapter.panes.size()) {
                    Pane pane = TungstenActivity.this.mCollectionPagerAdapter.panes.get(position);
                    TungstenActivity.paneDB.delete(pane.id);
                    TungstenActivity.this.mCollectionPagerAdapter.panes.remove(pane);
                }
                TungstenActivity.this.mCollectionPagerAdapter.notifyDataSetChanged();
            }
        });
        alert.setNegativeButton(R.string.label_no, (DialogInterface.OnClickListener) null);
        alert.show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tungsten, menu);
        return true;
    }

    public static class CollectionPagerAdapter extends FragmentStatePagerAdapter {
        public ArrayList<Pane> panes = TungstenActivity.paneDB.getAll();

        public CollectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public Fragment getItem(int position) {
            if (this.panes.size() != 0) {
                return ObjectFragment.newInstance(this.panes.get(position));
            }
            if (position == 0) {
                return new EmptyFragment();
            }
            return new HelpFragment();
        }

        public int getItemPosition(Object object) {
            return -2;
        }

        public int getCount() {
            if (this.panes.size() == 0) {
                return 2;
            }
            return this.panes.size();
        }

        public CharSequence getPageTitle(int position) {
            if (this.panes.size() != 0) {
                return this.panes.get(position).title;
            }
            if (position == 0) {
                return TungstenActivity.appContext.getString(R.string.empty_pane_title);
            }
            return TungstenActivity.appContext.getString(R.string.help_pane_title);
        }
    }

    public static class EmptyFragment extends Fragment {
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.empty_fragment_object, container, false);
        }
    }

    public static class HelpFragment extends Fragment {
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.help_fragment_object, container, false);
        }
    }

    public static class ObjectFragment extends Fragment {
        public static final String PANE = "pane";
        String yaml;

        static ObjectFragment newInstance(Pane pane) {
            ObjectFragment fragment = new ObjectFragment();
            Bundle args = new Bundle();
            args.putString(PANE, pane.yaml);
            fragment.setArguments(args);
            return fragment;
        }

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.yaml = getArguments() != null ? getArguments().getString(PANE) : "[]";
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            List<UIButton> buttons;
            Parser parser = new Parser();
            View rootView = inflater.inflate(R.layout.fragment_collection_object, container, false);
            try {
                buttons = parser.parseButtons(this.yaml);
            } catch (RuntimeException e) {
                Toast.makeText(rootView.getContext(), R.string.yaml_error, Toast.LENGTH_SHORT).show();
                buttons = new ArrayList<>();
            }
            TableLayout tl = (TableLayout) rootView.findViewById(R.id.fragment_layout);
            TableRow tr = new TableRow(rootView.getContext());
            int spans = 0;
            for (UIButton button : buttons) {
                TableRow.LayoutParams params = new TableRow.LayoutParams();
                params.span = button.span;
                tr.addView(makeButton(rootView, button), params);
                spans += button.span;
                if (spans >= 6) {
                    tl.addView(tr, new TableLayout.LayoutParams(-1, -2));
                    tr.setLayoutParams(new TableRow.LayoutParams(-1, -2));
                    tr = new TableRow(rootView.getContext());
                    tr.setLayoutParams(new TableRow.LayoutParams(-1, -2));
                    spans = 0;
                }
            }
            if (spans > 0) {
                tl.addView(tr, new TableLayout.LayoutParams(-1, -2));
            }
            return rootView;
        }

        /* access modifiers changed from: protected */
        @SuppressLint({"InlinedApi"})
        public Button makeButton(View view, final UIButton button) {
            Button myButton = new Button(view.getContext(), (AttributeSet) null);
            myButton.setText(button.label);
            myButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    new HTTPCallTask(view.getContext()).execute(new String[]{button.URL});
                }
            });
            return myButton;
        }
    }
}
