package com.stochastictechnologies.tungsten;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.yaml.snakeyaml.Yaml;

public class Parser {
    private Yaml parser = new Yaml();

    private Pane parseDocument(Object document) {
        Pane pane = new Pane();
        pane.title = (String) ((HashMap) document).get("title");
        pane.yaml = this.parser.dump(((HashMap) document).get("ui"));
        return pane;
    }

    public ArrayList<Pane> parseFile(String yaml) {
        ArrayList<Pane> panes = new ArrayList<>();
        for (Object document : this.parser.loadAll(yaml)) {
            try {
                panes.add(parseDocument(document));
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
        return panes;
    }

    public List<UIButton> parseButtons(String yaml) {
        String label;
        String url;
        Integer span;
        Integer offset;
        ArrayList<UIButton> buttons = new ArrayList<>();
        for (HashMap<String, Object> item : (List<HashMap<String, Object>>) this.parser.load(yaml)) {
            if (item.get("label") != null) {
                label = (String) item.get("label");
            } else {
                label = "Button";
            }
            if (item.get("url") != null) {
                url = (String) item.get("url");
            } else {
                url = "http://example.com";
            }
            if (item.get("span") != null) {
                span = (Integer) item.get("span");
            } else {
                span = 6;
            }
            if (item.get("offset") != null) {
                offset = (Integer) item.get("offset");
            } else {
                offset = 0;
            }
            buttons.add(new UIButton(label, url, span.intValue(), offset.intValue()));
        }
        return buttons;
    }
}
