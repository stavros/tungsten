package com.stochastictechnologies.tungsten;

/* compiled from: Panes */
class Pane {
    public int id;
    public String title;
    public String yaml;

    public Pane() {
    }

    public Pane(int id2, String title2, String yaml2) {
        this.id = id2;
        this.title = title2;
        this.yaml = yaml2;
    }
}
