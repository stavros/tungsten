package com.stochastictechnologies.tungsten;

/* compiled from: Panes */
class UIButton {
    public String URL;
    public String label;
    public int offset;
    public int span;

    public UIButton() {
    }

    public UIButton(String label2, String URL2, int span2, int offset2) {
        this.label = label2;
        this.URL = URL2;
        this.span = span2;
        this.offset = offset2;
    }
}
